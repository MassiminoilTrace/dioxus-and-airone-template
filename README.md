# Dioxus and Airone template

This template combines [Dioxus](https://dioxuslabs.com/) framework with [airone](https://gitlab.com/MassiminoilTrace/airone) light "database" library.

This combination enables you to easily persist your app state automatically, while maintaining the ease of use of `Vec<T>` to access your data.

