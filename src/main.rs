#![allow(non_snake_case)]
#![windows_subsystem = "windows"]

// Import Dioxus
use dioxus::prelude::*;

// Import and initialize airone
use airone::prelude::*;
use airone::airone_derive::AironeDbDerive;
airone_init!();


// Define the model to persist
#[derive(AironeDbDerive)]
#[derive(PartialEq, Clone)]
struct MyDataModel
{
    uuid: String,
    field_1: String,
    field_2: i32
}


fn main() {
    dioxus::launch(App);
}


#[component]
fn App() -> Element
{
    let mut database: Signal<AironeDb<MyDataModel>, SyncStorage> = use_signal_sync(
        ||{
            AironeDb::new()
        }
    );
    rsx!(
        p{
            "Each modification, each time you alter the value, gets automatically rapidly saved :)"
        }
        small {"Hint: check what happens to the newly generated CSV files in the project folder as you click the buttons"}

        div{
            button
            {
                onclick: move|_|
                {
                    database.write().clear();
                },
                "Clear list"
            }
            button {
                onclick: move|_|
                {
                    // Add an element like it was a simple Vec<T>
                    database.write().push(
                        MyDataModel{
                            uuid: uuid::Uuid::new_v4().to_string(),
                            field_1: String::from("FOO BAR"),
                            field_2: 0
                        }
                    );
                },
                "Add test element element"
            }
        }
        hr{}
        ul {
            for child in database.read().get_all()
            {
                ChildView{
                    key: "{child.uuid}",
                    child_data: child.clone(),
                    on_delete: move |uuid: String|
                    {
                        // Easily filter the desired id and delete its element
                        database.write()
                            .build_query_mut()
                            .filter(|el|el.uuid==uuid)
                            .delete();
                        // <-- At this point, the change has already
                        // been written to file automatically
                    }
                }
            }
        }
    )
}


#[component]
fn ChildView(child_data: MyDataModel, on_delete: EventHandler<String>) -> Element
{
    rsx!(
        li
        {
            button{
                onclick:move |_|
                {
                    on_delete.call(child_data.uuid.clone());
                },
                "X"
            }
            "{child_data.field_1}, {child_data.field_2}, uuid: {child_data.uuid}"
        }
    )
}